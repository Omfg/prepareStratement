import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.Calendar;

/**
 * Created by omfg on 11.12.16.
 */
public class Main {
    private static final String URL = "jdbc:mysql://192.168.1.106:3306/mydbtest";
    private static final String USERNAME = "omfg";
    private static final String PASSWORD = "101541";

    private static final String INSERT_NEW = "INSERT INTO dish VALUES(?,?,?,?,?,?,?)";
    private static final String GET_ALL = "SELECT * FROM dish";
    private static final String DELETE = "DELETE FROM dish WHERE id=?";


    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try{
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            for (int i = 0; i < 1000; i++) {



            preparedStatement = connection.prepareStatement(INSERT_NEW);
            preparedStatement.setInt(1,1+i);
            preparedStatement.setString(2,"Inserted title"+i);
            preparedStatement.setString(3,"Inserted desc"+i);
            preparedStatement.setFloat(4,0.2f+i);
            preparedStatement.setBoolean(5,true);
            preparedStatement.setDate(6,new Date(Calendar.getInstance().getTimeInMillis()));
            preparedStatement.setBlob(7,new FileInputStream("res/smile.png"));
            preparedStatement.execute();
            System.out.println(i);
            }
//            for (int i = 0; i < 1000; i++) {

//
//                preparedStatement = connection.prepareStatement(DELETE);
//                preparedStatement.setInt(1, i);
//                preparedStatement.executeUpdate();
//            }
            preparedStatement = connection.prepareStatement(GET_ALL);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
             int id = resultSet.getInt("id");
             String title = resultSet.getString("title");
             String desc = resultSet.getString("description");
             float rating = resultSet.getFloat("raiting");
             boolean published = resultSet.getBoolean("published");
             Date date = resultSet.getDate("created");
             byte[] icon = resultSet.getBytes("icon");


                System.out.println("id: "+id+", title: "+title+", desc: "+desc+", raiting: "+ rating+""
                        +", published: "+published+", date: "+date+", icon length:"+icon.length);
            }




        }catch (SQLException e){e.printStackTrace();}
        catch (FileNotFoundException e)
 {
            e.printStackTrace();
        }
        finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }
}
